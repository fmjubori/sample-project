#include <opencv2/opencv.hpp>

int main()
{
	cv::Vec bgrPixel(40, 158, 16);
	cv::Mat bgr(bgrPixel);

	cv::Mat ycb;
	cv::cvtColor(bgr, ycb, COLOR_BGR2YCrCb);
	cv::Vec ycbPixel(ycb.at<Vec3b>(0,0));

	int thresh = 40;
	cv::Scalar minYCB = cv::Scalar(
		ycbPixel.val[0] – thresh,
		ycbPixel.val[1] – thresh,
		ycbPixel.val[2] – thresh);
	cv::Scalar maxYCB = cv::Scalar(
		ycbPixel.val[0] + thresh,
		ycbPixel.val[1] + thresh,
		ycbPixel.val[2] + thresh);

	cv::Mat maskYCB, resultYCB;
	cv::inRange(ycb, minYCB, maxYCB, maskYCB);
	cv::bitwise_and(ycb, ycb, resultYCB, maskYCB);

	cv::Mat kernel = cv::Mat::ones(5,5, CV_64F);
	kernel = kernel / 25;
	cv::Mat img;
	cv::filter2D(image, img, -1 , kernel, cv::Point(-1, -1), 0, 4);

	cv::imwrite("result.ppm", img);
}
